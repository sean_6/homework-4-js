const validateNumber = number => (number !== null && number !== '' && !isNaN(number));
const getNumber = (message = 'Enter your number') => {
    let userNumber;

    do {
        userNumber = prompt(message);
    } while (!validateNumber(userNumber));

    return +userNumber;
}

const calcResult = (mathOperation) => {

    const a = getNumber('Enter first number');
    const b = getNumber('Enter second number');

    switch (mathOperation) {

        case '+': {
            return a + b;
        }

        case '-': {
            return a - b;
        }

        case '*': {
            return a * b;
        }

        case '/': {
            return a / b;
        }
    }
}

let mathOperator;

do {
    mathOperator = prompt('Enter mathematical operator: +, -, *, /');

} while (mathOperator !== '+' && mathOperator !== '-' && mathOperator !== '*' && mathOperator !== '/');

console.log(calcResult(mathOperator));
